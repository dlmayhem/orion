#+author: Orion
#+title: Mysterium Cosmographicum
#+date: 2020-09-30

* On Being and Becoming
#+begin_quote
Faceless King before the time,\\
In your frozen throne beyond any place.\\
Thou who confer meaning from chaos,\\
Show me the secrets of what always remains.

How could my words return to Thee?\\ 
For the echo of my voice will be dead at dawn,\\
And my troubled eyes can't defeat the mist,\\
So how could I walk through the eternal lawns?

A perfect model like no other can be,\\
Strictly crafted by the hands of doom.

Majestic the realm of the undying beliefs.\\
Of what always is and never becomes.\\
But perceptual fields are confusing and weak,\\
Where everything is drawn in upsetting tones.

Only by force such essences could\\ 
Be unified in one only being. 

Not needed of eyes when there's nothing to see,\\
And the absence of sound does not precise ears.\\
No arms, no legs, cause there's nothing to grip,\\
Turned to itself over years and years.

Time there was and there will be,\\
Every experience and every deed,\\
Pale reflections of the Highest Soul.\\
That is the nature of what can be told.
#+end_quote

* Soul Procession
#+begin_quote
Lonely One, cause of being.\\ 
All the things and none at once.\\
Every substance is preserved in Him\\
For there is nothing beyond his walls.

Turn around! Can't you see?\\
Nothing is different!\\
Procession of souls.

Self-contemplation, identity,\\ 
Terms in relation, but nothing outside.\\
Multiplicity of the same.\\
Everything is tied up in this way of life.

Turn around! Can't you see?\\ 
Nothing is different!\\
Procession of souls.

Pulling knowledge with a high desire.\\
Instantiating the ideal forms.\\ 
Living infinity as an embodied God,\\
Last frontier before the storm.

Turn around! can't you see?\\
Nothing is different!\\
Procession of souls.
#+end_quote

* The Way of Truth
#+begin_quote
Noble steeds lead me by\\
Oniric paths to reach the Night.\\
There I can see the nature of things.

Stay apart from the mortal way of thought!\\
That is what She said to me.\\
The truth appears when you stare from the dark\\
Cause the eyes of the Soul don't need the light.

Space shall not take place.\\
Time shall never come.\\
Motion, nonsense.\\
Change, delusion.

For it can't be\\
what can't be thought,\\
And it can't die\\
Who wasn't born.

Space shall not take place.\\
Time shall never come.\\
Motion, nonsense.\\
Change, delusion.

It doesn't matter where I start\\
Cause there I will return again.\\
That is the road I have walked,\\
Which has no beginning or end. 
#+end_quote

* The Unreality of Time
#+begin_quote
/Instrumental/
#+end_quote

* Unreasoned Perceptions
#+begin_quote
What is the reason for\\
All the things you trust?\\ 
All you take for granted,\\
All your sight can reach,\\
How sure can you be\\
That it is all there?

Ignorance of the true beliefs.\\
Unreasoned perceptions!

Those who live in a cave\\
Speculating in vain\\
The only reward they get\\ 
Are shadows on the wall.\\
Break the chains of the senses!\\
Wisdom lies beyond.

Ignorance of the true beliefs.\\
Unreasoned perceptions!

If you can't be awake\\
Outside an infernal dream;\\
If you can't deceive\\
To the Great Deceiver,\\ 
How can you recognize\\
What is fake and what is real?

Ignorance of the true beliefs.\\
Unreasoned perceptions!
#+end_quote

* Epínomis
#+begin_quote
Vast is the view of the Welkin\\
And small the eyes that see.\\
How can the beast capture his greatness\\
In vain and empty words?\\
Many arts concerning the fool,\\
Making their misery aside;\\
Which one of them will put the man\\
In harmony with The One?

The leaves fall and grow again,\\
The river flows not filling the seas.\\
Hunts the wolf with usual wisdom,\\
Giving his life to the crows.\\
Passes the man naming the world,\\
Imprisoning what always escapes.\\
The tides roar, the fire rages,\\
But the Spheres will ever go on.

How can even rule\\
Who never felt the Absolute?

Wise those who stare\\
By the grace of the Number!

Wise are the eternal laws of the Sky.\\
Wise are the stars and their sacred path.\\
Wise is the art of order and measure,\\
Whose rules will never die.
#+end_quote

* Uttermost Absence
#+begin_quote
Vain illusion is all we have!\\
There is no gain in so much pain.\\
Some will born, some will die,\\
And what could change under the sky?

There is no word to express\\
How absurd is this world.\\
There is no sin to confess\\
In this land of doom. 

A time to kill, a time to live,\\
A time to fear, a time to rest.\\
A moon to sow, another to reap,\\
And what is new under the sun?

There is no word to express\\
How absurd is this world.\\
There is no sin to confess\\
In this land of doom.

Same is the fate that awaits us all,\\
Both the saint and the ungodly one.\\
There is no judgment beneath the ground\\
Beyond the one dictated by worms.

There is no word to express\\
How absurd is this world.\\
There is no sin to confess\\
In this land of doom.
#+end_quote

* The Wanderer
#+begin_quote
I have seen too many things\\
That turn a man in despair.\\
Revelations from the depths of time,\\
Echoes of death and pain.\\
I never hated my enemy\\
While my blade cut off his head,\\
Or does the wolf hate its prey\\
When it lies into his jaws?

I have walked too many roads\\
Alone between skies and earth.\\
Forging my soul in the flames of horror,\\
Where others embrace the fear.\\
I never had mercy\\ 
While my fists crushed their bones,\\
Or does the storm have remorses\\
When it smashes entire towns?

I have stared at the face of death\\
Weilding my sword against her neck\\ 
With the eternal wish to descend to hell,\\
But I am in The Road again.\\
I never felt affection\\ 
For the things of this world.\\
Cannot gold fill my spirit,\\
Nor can the Gods!
#+end_quote
